var readline = require('readline');
var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

var stock = 0;

function substringActionValue (line){
  //split the string into the useful parts, abs on the path to reduce the risk of messing up the values.
  var action = line.substr(0, 1);
  var value = Math.abs(line.substr(1));
  return [action, value];
}

/*
a function for keeping track of the current stock and return
*/
function perfAction(action, value){
  // a sanity check of the value to ensure that is is numeric.
  if (!/^\d+$/.test(value)){
    return [-1, `non numeric value in value part of string`]
  }

  //switch in the action to handle Logistics and sales, with "error handling"
  switch (action){
    case "L": //Logistics
      if (value){
        increaseStock(value);
        return [0, `added`];
      } else {
        return [0, `current stock : ${getStock()}`];
      }
      break;
    case "S": //sales
      if (value){
        reduceStock(value);
        return [0, `reduced`];
      } else { //error handling for missing number of items to sell
        console.log('here');
        return [-1, `missing value, pls enter number of units to sell`];
      }
      break;
    default: //error handling for entring wrong action type
      return [-1, `No such option. Please enter another: `];
    }
}

function getStock(){
  return stock;
}

function reduceStock(value){
  stock -= ((getStock() - value >= 0) ? value : getStock());
}

function increaseStock(value){
  stock += value;
}

var recursiveAsyncReadLine = function () {
    rl.question("\n"
+ " * Att sälja x antal. Görs genom att man skriver S och så antalet + enter, t.ex. ”S5”. \n"
+ " * Att inleverera x antal. Görs genom att man skriver I och så antalet + enter, t.ex. ”I5”.\n"
+ " * Visa aktuellt lager. Görs genom att man skriver ”L” + enter.\n"
+ "\n"
+ " ** Ctrl + c för att avsluta\n\n > "
        , function (line) {
          var ss = substringActionValue(line);
          var logString = perfAction(ss[0], ss[1]);
          var strColor = ((logString[0] == 0) ? `\x1b[36m%s\x1b[0m` : `\x1b[1;31m\x1b[0;31m` );
          console.log(strColor, logString[1]);
          recursiveAsyncReadLine(); //Calling this function again to ask new question
    });
};

recursiveAsyncReadLine();
